import React from 'react';
import { graphql } from 'react-relay';
import { useConfig } from '@saastack/core';
import { useQuery } from '@saastack/relay';
import { useDidMountEffect } from '@saastack/utils';
import { ErrorComponent, Loading } from '@saastack/components';
import {
    DepartmentPageQuery,
    DepartmentPageQueryResponse,
    DepartmentPageQueryVariables,
} from '../__generated__/DepartmentPageQuery.graphql';
import DepartmentMaster from '../components/DepartmentMaster';

interface Props {}

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)
    }
`;

const DepartmentPage: React.FC<Props> = ({ ...props }) => {
    const { companyId }  = useConfig();
    const variables: DepartmentPageQueryVariables = { parent: companyId };
    const { data, loading, error, refetch } = useQuery<DepartmentPageQuery>(query, variables);

    useDidMountEffect(() => {
        refetch();
    }, [companyId]);

    if(loading) {
        return <Loading/>
    }
    if(error) {
        return <ErrorComponent error={error}/>
    }

    return (
        <DepartmentMaster
            parent={companyId!}
            departments={data!}
        />
    );
}

export default DepartmentPage;