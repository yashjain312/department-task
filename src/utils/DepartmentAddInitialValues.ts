
const departmentAddInitialValues = {
    id: '',
    name: '',
    description: '',
    roleIds: [],
}

export default departmentAddInitialValues;