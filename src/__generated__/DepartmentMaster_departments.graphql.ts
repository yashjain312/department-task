/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentMaster_departments = {
    readonly departments: {
        readonly department: ReadonlyArray<{
            readonly id: string;
            readonly name: string;
        }>;
    };
    readonly " $refType": "DepartmentMaster_departments";
};
export type DepartmentMaster_departments$data = DepartmentMaster_departments;
export type DepartmentMaster_departments$key = {
    readonly " $data"?: DepartmentMaster_departments$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentMaster_departments">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "parent"
    }
  ],
  "kind": "Fragment",
  "metadata": null,
  "name": "DepartmentMaster_departments",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Variable",
          "name": "parent",
          "variableName": "parent"
        }
      ],
      "concreteType": "ListDepartmentResponse",
      "kind": "LinkedField",
      "name": "departments",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Department",
          "kind": "LinkedField",
          "name": "department",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};
(node as any).hash = '69e9c85a545806b2d509dbc6e02d0072';
export default node;
