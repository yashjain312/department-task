/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type UpdateDepartmentInput = {
    clientMutationId?: string | null;
    department?: DepartmentInput | null;
    updateMask?: FieldMaskInput | null;
};
export type DepartmentInput = {
    description?: string | null;
    id?: string | null;
    metadata?: string | null;
    name?: string | null;
};
export type FieldMaskInput = {
    paths?: Array<string | null> | null;
};
export type UpdateDepartmentMutationVariables = {
    input?: UpdateDepartmentInput | null;
};
export type UpdateDepartmentMutationResponse = {
    readonly updateDepartment: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly id: string;
            readonly name: string;
            readonly description: string;
        };
    };
};
export type UpdateDepartmentMutation = {
    readonly response: UpdateDepartmentMutationResponse;
    readonly variables: UpdateDepartmentMutationVariables;
};



/*
mutation UpdateDepartmentMutation(
  $input: UpdateDepartmentInput
) {
  updateDepartment(input: $input) {
    clientMutationId
    payload {
      id
      name
      description
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "UpdateDepartmentPayload",
    "kind": "LinkedField",
    "name": "updateDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Department",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "UpdateDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "UpdateDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "5ecab05b0a5cbe2f29a13d4ddcedb0cc",
    "id": null,
    "metadata": {},
    "name": "UpdateDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation UpdateDepartmentMutation(\n  $input: UpdateDepartmentInput\n) {\n  updateDepartment(input: $input) {\n    clientMutationId\n    payload {\n      id\n      name\n      description\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = 'e289aca752622f2b723f1c77eb05b9fa';
export default node;
