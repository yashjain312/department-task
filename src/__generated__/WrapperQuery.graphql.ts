/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type RestrictionTypes = "ATTRIBUTE" | "CYCLIC_NUMBER_LIMIT" | "FEATURE_RPC" | "MODULE" | "NUMBER_LIMIT" | "UNSPECIFIED_RESTRICTION" | "%future added value";
export type WrapperQueryVariables = {};
export type WrapperQueryResponse = {
    readonly viewer: {
        readonly id: string;
        readonly firstName: string;
        readonly lastName: string;
        readonly email: string;
        readonly profileImage: {
            readonly thumbImage: string;
        } | null;
        readonly preferences: {
            readonly dateFormat: string;
            readonly language: string;
            readonly timeFormat: string;
            readonly timezone: string;
            readonly uiInfo: string;
        } | null;
        readonly userRoles: {
            readonly role: ReadonlyArray<{
                readonly levelDetails: {
                    readonly id?: string;
                } | null;
                readonly levelId: string;
                readonly roleId: string;
                readonly role: {
                    readonly id: string;
                    readonly roleName: string;
                    readonly isDefault: boolean;
                    readonly priority: number;
                    readonly level: string;
                    readonly moduleRoles: ReadonlyArray<{
                        readonly name: string;
                    }>;
                } | null;
            }>;
        } | null;
        readonly groups: ReadonlyArray<{
            readonly id: string;
            readonly name: string;
            readonly companies: ReadonlyArray<{
                readonly id: string;
                readonly title: string;
                readonly displayName: string;
                readonly metadata: string | null;
                readonly gallery: {
                    readonly default: {
                        readonly thumbImage: string;
                    } | null;
                } | null;
                readonly address: {
                    readonly country: string;
                } | null;
                readonly companySettings: {
                    readonly navMenus: ReadonlyArray<string>;
                    readonly aliases: string | null;
                } | null;
                readonly apps: ReadonlyArray<{
                    readonly id: string;
                    readonly appTypeId: string;
                    readonly name: string;
                    readonly active: boolean;
                    readonly serviceModules: ReadonlyArray<string>;
                }>;
                readonly locations: {
                    readonly edges: ReadonlyArray<{
                        readonly node: {
                            readonly id: string;
                            readonly name: string;
                            readonly preference: {
                                readonly currency: string;
                            } | null;
                            readonly active: boolean;
                        } | null;
                    }>;
                } | null;
                readonly licenseWallet: {
                    readonly license: {
                        readonly moduleLicenses: ReadonlyArray<{
                            readonly enabled: boolean;
                            readonly freeLimit: number;
                            readonly licenseLimit: number;
                            readonly maxAddonLimit: number;
                            readonly maxLimit: number;
                            readonly moduleName: string;
                            readonly paidAddonLimit: number;
                            readonly restrictionSlugs: ReadonlyArray<string>;
                            readonly restrictionType: RestrictionTypes;
                            readonly rpcs: ReadonlyArray<string>;
                            readonly slug: string;
                            readonly totalLimit: number;
                        }>;
                    } | null;
                } | null;
            }>;
        }>;
    };
};
export type WrapperQuery = {
    readonly response: WrapperQueryResponse;
    readonly variables: WrapperQueryVariables;
};



/*
query WrapperQuery {
  viewer {
    id
    firstName
    lastName
    email
    profileImage {
      thumbImage
    }
    preferences {
      dateFormat
      language
      timeFormat
      timezone
      uiInfo
      id
    }
    userRoles {
      role {
        levelDetails {
          __typename
          ... on Location {
            id
          }
          ... on Company {
            id
          }
          ... on Group {
            id
          }
          ... on Employee {
            id
          }
          ... on Node {
            __isNode: __typename
            id
          }
        }
        levelId
        roleId
        role {
          id
          roleName
          isDefault
          priority
          level
          moduleRoles {
            name
          }
        }
      }
      id
    }
    groups {
      id
      name
      companies {
        id
        title
        displayName
        metadata
        gallery {
          default {
            thumbImage
          }
        }
        address {
          country
        }
        companySettings {
          navMenus
          aliases(locale: "en-US")
          id
        }
        apps {
          id
          appTypeId
          name
          active
          serviceModules
        }
        locations(first: 500) {
          edges {
            node {
              id
              name
              preference {
                currency
                id
              }
              active
            }
          }
        }
        licenseWallet {
          license {
            moduleLicenses {
              enabled
              freeLimit
              licenseLimit
              maxAddonLimit
              maxLimit
              moduleName
              paidAddonLimit
              restrictionSlugs
              restrictionType
              rpcs
              slug
              totalLimit
            }
            id
          }
          id
        }
      }
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "firstName",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "lastName",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "email",
  "storageKey": null
},
v4 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "thumbImage",
    "storageKey": null
  }
],
v5 = {
  "alias": null,
  "args": null,
  "concreteType": "GalleryItem",
  "kind": "LinkedField",
  "name": "profileImage",
  "plural": false,
  "selections": (v4/*: any*/),
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "dateFormat",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "language",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "timeFormat",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "timezone",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "uiInfo",
  "storageKey": null
},
v11 = [
  (v0/*: any*/)
],
v12 = {
  "kind": "InlineFragment",
  "selections": (v11/*: any*/),
  "type": "Location",
  "abstractKey": null
},
v13 = {
  "kind": "InlineFragment",
  "selections": (v11/*: any*/),
  "type": "Company",
  "abstractKey": null
},
v14 = {
  "kind": "InlineFragment",
  "selections": (v11/*: any*/),
  "type": "Group",
  "abstractKey": null
},
v15 = {
  "kind": "InlineFragment",
  "selections": (v11/*: any*/),
  "type": "Employee",
  "abstractKey": null
},
v16 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "levelId",
  "storageKey": null
},
v17 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "roleId",
  "storageKey": null
},
v18 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v19 = {
  "alias": null,
  "args": null,
  "concreteType": "Role",
  "kind": "LinkedField",
  "name": "role",
  "plural": false,
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "roleName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isDefault",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "priority",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "level",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ModuleRole",
      "kind": "LinkedField",
      "name": "moduleRoles",
      "plural": true,
      "selections": [
        (v18/*: any*/)
      ],
      "storageKey": null
    }
  ],
  "storageKey": null
},
v20 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "title",
  "storageKey": null
},
v21 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "displayName",
  "storageKey": null
},
v22 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "metadata",
  "storageKey": null
},
v23 = {
  "alias": null,
  "args": null,
  "concreteType": "Gallery",
  "kind": "LinkedField",
  "name": "gallery",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "GalleryItem",
      "kind": "LinkedField",
      "name": "default",
      "plural": false,
      "selections": (v4/*: any*/),
      "storageKey": null
    }
  ],
  "storageKey": null
},
v24 = {
  "alias": null,
  "args": null,
  "concreteType": "Address",
  "kind": "LinkedField",
  "name": "address",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "country",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v25 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "navMenus",
  "storageKey": null
},
v26 = {
  "alias": null,
  "args": [
    {
      "kind": "Literal",
      "name": "locale",
      "value": "en-US"
    }
  ],
  "kind": "ScalarField",
  "name": "aliases",
  "storageKey": "aliases(locale:\"en-US\")"
},
v27 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "active",
  "storageKey": null
},
v28 = {
  "alias": null,
  "args": null,
  "concreteType": "App",
  "kind": "LinkedField",
  "name": "apps",
  "plural": true,
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "appTypeId",
      "storageKey": null
    },
    (v18/*: any*/),
    (v27/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "serviceModules",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v29 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 500
  }
],
v30 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "currency",
  "storageKey": null
},
v31 = {
  "alias": null,
  "args": null,
  "concreteType": "ModuleLicenses",
  "kind": "LinkedField",
  "name": "moduleLicenses",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "enabled",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "freeLimit",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "licenseLimit",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "maxAddonLimit",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "maxLimit",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "moduleName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "paidAddonLimit",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "restrictionSlugs",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "restrictionType",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "rpcs",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "slug",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "totalLimit",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "WrapperQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "UserProfile",
        "kind": "LinkedField",
        "name": "viewer",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          (v1/*: any*/),
          (v2/*: any*/),
          (v3/*: any*/),
          (v5/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "UserPreference",
            "kind": "LinkedField",
            "name": "preferences",
            "plural": false,
            "selections": [
              (v6/*: any*/),
              (v7/*: any*/),
              (v8/*: any*/),
              (v9/*: any*/),
              (v10/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserRole",
            "kind": "LinkedField",
            "name": "userRoles",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "UserRoleRole",
                "kind": "LinkedField",
                "name": "role",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "levelDetails",
                    "plural": false,
                    "selections": [
                      (v12/*: any*/),
                      (v13/*: any*/),
                      (v14/*: any*/),
                      (v15/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v16/*: any*/),
                  (v17/*: any*/),
                  (v19/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Group",
            "kind": "LinkedField",
            "name": "groups",
            "plural": true,
            "selections": [
              (v0/*: any*/),
              (v18/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Company",
                "kind": "LinkedField",
                "name": "companies",
                "plural": true,
                "selections": [
                  (v0/*: any*/),
                  (v20/*: any*/),
                  (v21/*: any*/),
                  (v22/*: any*/),
                  (v23/*: any*/),
                  (v24/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "CompanySetting",
                    "kind": "LinkedField",
                    "name": "companySettings",
                    "plural": false,
                    "selections": [
                      (v25/*: any*/),
                      (v26/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v28/*: any*/),
                  {
                    "alias": null,
                    "args": (v29/*: any*/),
                    "concreteType": "ListLocationResponse",
                    "kind": "LinkedField",
                    "name": "locations",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "LocationNode",
                        "kind": "LinkedField",
                        "name": "edges",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Location",
                            "kind": "LinkedField",
                            "name": "node",
                            "plural": false,
                            "selections": [
                              (v0/*: any*/),
                              (v18/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "LocationPreference",
                                "kind": "LinkedField",
                                "name": "preference",
                                "plural": false,
                                "selections": [
                                  (v30/*: any*/)
                                ],
                                "storageKey": null
                              },
                              (v27/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": "locations(first:500)"
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "LicenseWallet",
                    "kind": "LinkedField",
                    "name": "licenseWallet",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "License",
                        "kind": "LinkedField",
                        "name": "license",
                        "plural": false,
                        "selections": [
                          (v31/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "WrapperQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "UserProfile",
        "kind": "LinkedField",
        "name": "viewer",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          (v1/*: any*/),
          (v2/*: any*/),
          (v3/*: any*/),
          (v5/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "UserPreference",
            "kind": "LinkedField",
            "name": "preferences",
            "plural": false,
            "selections": [
              (v6/*: any*/),
              (v7/*: any*/),
              (v8/*: any*/),
              (v9/*: any*/),
              (v10/*: any*/),
              (v0/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "UserRole",
            "kind": "LinkedField",
            "name": "userRoles",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "UserRoleRole",
                "kind": "LinkedField",
                "name": "role",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "levelDetails",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "__typename",
                        "storageKey": null
                      },
                      (v12/*: any*/),
                      (v13/*: any*/),
                      (v14/*: any*/),
                      (v15/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": (v11/*: any*/),
                        "type": "Node",
                        "abstractKey": "__isNode"
                      }
                    ],
                    "storageKey": null
                  },
                  (v16/*: any*/),
                  (v17/*: any*/),
                  (v19/*: any*/)
                ],
                "storageKey": null
              },
              (v0/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Group",
            "kind": "LinkedField",
            "name": "groups",
            "plural": true,
            "selections": [
              (v0/*: any*/),
              (v18/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Company",
                "kind": "LinkedField",
                "name": "companies",
                "plural": true,
                "selections": [
                  (v0/*: any*/),
                  (v20/*: any*/),
                  (v21/*: any*/),
                  (v22/*: any*/),
                  (v23/*: any*/),
                  (v24/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "CompanySetting",
                    "kind": "LinkedField",
                    "name": "companySettings",
                    "plural": false,
                    "selections": [
                      (v25/*: any*/),
                      (v26/*: any*/),
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v28/*: any*/),
                  {
                    "alias": null,
                    "args": (v29/*: any*/),
                    "concreteType": "ListLocationResponse",
                    "kind": "LinkedField",
                    "name": "locations",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "LocationNode",
                        "kind": "LinkedField",
                        "name": "edges",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Location",
                            "kind": "LinkedField",
                            "name": "node",
                            "plural": false,
                            "selections": [
                              (v0/*: any*/),
                              (v18/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "LocationPreference",
                                "kind": "LinkedField",
                                "name": "preference",
                                "plural": false,
                                "selections": [
                                  (v30/*: any*/),
                                  (v0/*: any*/)
                                ],
                                "storageKey": null
                              },
                              (v27/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": "locations(first:500)"
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "LicenseWallet",
                    "kind": "LinkedField",
                    "name": "licenseWallet",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "License",
                        "kind": "LinkedField",
                        "name": "license",
                        "plural": false,
                        "selections": [
                          (v31/*: any*/),
                          (v0/*: any*/)
                        ],
                        "storageKey": null
                      },
                      (v0/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "a8f3ed94343b55b907423e3f306055d0",
    "id": null,
    "metadata": {},
    "name": "WrapperQuery",
    "operationKind": "query",
    "text": "query WrapperQuery {\n  viewer {\n    id\n    firstName\n    lastName\n    email\n    profileImage {\n      thumbImage\n    }\n    preferences {\n      dateFormat\n      language\n      timeFormat\n      timezone\n      uiInfo\n      id\n    }\n    userRoles {\n      role {\n        levelDetails {\n          __typename\n          ... on Location {\n            id\n          }\n          ... on Company {\n            id\n          }\n          ... on Group {\n            id\n          }\n          ... on Employee {\n            id\n          }\n          ... on Node {\n            __isNode: __typename\n            id\n          }\n        }\n        levelId\n        roleId\n        role {\n          id\n          roleName\n          isDefault\n          priority\n          level\n          moduleRoles {\n            name\n          }\n        }\n      }\n      id\n    }\n    groups {\n      id\n      name\n      companies {\n        id\n        title\n        displayName\n        metadata\n        gallery {\n          default {\n            thumbImage\n          }\n        }\n        address {\n          country\n        }\n        companySettings {\n          navMenus\n          aliases(locale: \"en-US\")\n          id\n        }\n        apps {\n          id\n          appTypeId\n          name\n          active\n          serviceModules\n        }\n        locations(first: 500) {\n          edges {\n            node {\n              id\n              name\n              preference {\n                currency\n                id\n              }\n              active\n            }\n          }\n        }\n        licenseWallet {\n          license {\n            moduleLicenses {\n              enabled\n              freeLimit\n              licenseLimit\n              maxAddonLimit\n              maxLimit\n              moduleName\n              paidAddonLimit\n              restrictionSlugs\n              restrictionType\n              rpcs\n              slug\n              totalLimit\n            }\n            id\n          }\n          id\n        }\n      }\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '355b77dc6b1822ad3a4a662b71d46e55';
export default node;
