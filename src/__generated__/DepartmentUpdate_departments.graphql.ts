/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentUpdate_departments = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly " $refType": "DepartmentUpdate_departments";
}>;
export type DepartmentUpdate_departments$data = DepartmentUpdate_departments;
export type DepartmentUpdate_departments$key = ReadonlyArray<{
    readonly " $data"?: DepartmentUpdate_departments$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentUpdate_departments">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DepartmentUpdate_departments",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = 'f081fe287bff8f863e42460a4b303e93';
export default node;
