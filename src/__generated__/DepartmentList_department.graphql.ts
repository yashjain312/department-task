/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type DepartmentList_department = ReadonlyArray<{
    readonly id: string;
    readonly name: string;
    readonly description: string;
    readonly " $refType": "DepartmentList_department";
}>;
export type DepartmentList_department$data = DepartmentList_department;
export type DepartmentList_department$key = ReadonlyArray<{
    readonly " $data"?: DepartmentList_department$data;
    readonly " $fragmentRefs": FragmentRefs<"DepartmentList_department">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "DepartmentList_department",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Department",
  "abstractKey": null
};
(node as any).hash = '86d5fa3d293d67cfad9e91d50f2a47c4';
export default node;
