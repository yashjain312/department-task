import { Trans } from '@lingui/macro';
import { useAlert } from '@saastack/core';
import { FormContainer } from '@saastack/layouts/containers';
import { FormContainerProps } from '@saastack/layouts/types';
import React, { useState } from 'react';
import { Variables } from 'react-relay';
// import { DesignationInput } from '../__generated__/CreateDesignationMutation.graphql'
import { useRelayEnvironment } from 'react-relay/hooks';
import DepartmentAddFormComponent from '../forms/DepartmentAddFormComponent'
import CreateDepartmentMutation from '../mutations/CreateDepartmentMutation';
import DepartmentAddValidation from '../utils/DepartmentAddValidation';
import departmentAddInitialValues from '../utils/DepartmentAddInitialValues';

interface Props extends Omit<FormContainerProps, 'formId'> {
    variables: Variables;
    onClose: () => void;
    onSuccess: () => void;
}

const formId = 'designation-add-form';

const DepartmentAdd: React.FC<Props> = ({roles, variables, onClose, onSuccess, ...props}) => {
    const environment = useRelayEnvironment();
    const showAlert = useAlert();
    const [loading, setLoading] = useState(false);

    const onError = (e: string) => {
        setLoading(false);
        showAlert(e, {
            variant: 'error',
        })
    }

    const handleSuccess = (response: DepartmentInput) => {
        setLoading(false);
        showAlert(<Trans>Department Added Successfully!</Trans>, {
            variant: 'info',
        });
        onSuccess();
    }
    
    const handleSubmit = (values: DepartmentInput) => {
        setLoading(true);
        const department = {
            ...values,
        }
        CreateDepartmentMutation.commit(environment, variables, department, {
            onSuccess: handleSuccess,
            onError,
        })
    };

    const initialValues = {
        ...departmentAddInitialValues,
    }

    return (
        <FormContainer
            open
            onClose={onClose}
            header={<Trans>New Department</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <DepartmentAddFormComponent
                onSubmit={handleSubmit}
                id={formId}
                initialValues={initialValues}
                validationSchema={DepartmentAddValidation}
            />
        </FormContainer>
    );
}

export default DepartmentAdd;