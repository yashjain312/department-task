import React, { useState } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import { Layout } from '@saastack/layouts';
import { Trans } from '@lingui/macro';
import DepartmentList from './DepartmentList';
import { ActionItem } from '@saastack/components/Actions';
import { AddOutlined } from '@material-ui/icons';
import DesignationAdd from './DepartmentAdd'
import { DepartmentMaster_departments } from '../__generated__/DepartmentMaster_departments.graphql';
import DepartmentUpdate from './DepartmentUpdate';
import DepartmentDelete from './DepartmentDelete';

interface Props {
    departments: DepartmentMaster_departments;
    parent: string;
}

const DepartmentMaster: React.FC<Props> = ({
    departments: {
        departments: {department: departments},
    },
    parent,
}) => {
    const [openAdd, setOpenAdd] = useState(false);
    const [openUpdate, setOpenUpdate] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const [selected, setSelected] = useState('');
    const variables = { parent };

    const handleAddSuccess = () => {
        setOpenAdd(false);
    }
    const handleUpdateSuccess = () => {
        setOpenUpdate(false);
    }
    const handleDeleteSuccess = () => {
        setOpenDelete(false);
    }
    const handleUpdateClose = () => {
        setOpenUpdate(false);
        setSelected('');
    }
    const handleDeleteClose = () =>{
        setOpenDelete(false);
        setSelected('');
    }

    const handleClick = (id: string, action: 'UPDATE' | 'DELETE') => {
        setSelected(id);
        if(action === 'UPDATE') {
            setOpenUpdate(true);
        } else {
            setOpenDelete(true);
        }
    }

    const actions: ActionItem[] = [
        {
            icon: AddOutlined,
            title: <Trans>Add</Trans>,
            onClick: () => setOpenAdd(true),
        }
    ]
    const list = <DepartmentList departments={departments} onClick={handleClick}/>

    return (
        <Layout header={<Trans>Departments</Trans>} col1={list} actions={actions}>
            {openAdd && (
                <DesignationAdd
                    variables={variables}
                    onClose={() => setOpenAdd(false)}
                    onSuccess={handleAddSuccess}
                />
            )}
            {openUpdate && (
                <DepartmentUpdate
                    id={selected}
                    onClose={handleUpdateClose}
                    onSuccess={handleUpdateSuccess}
                    departments={departments}
                />
            )}
            {openDelete && (
                <DepartmentDelete
                    variables={variables}
                    id={selected}
                    onClose={handleDeleteClose}
                    onSuccess={handleDeleteSuccess}
                />
            )}
        </Layout>
    );
}

export default createFragmentContainer(DepartmentMaster, {
    departments: graphql`
        fragment DepartmentMaster_departments on Query
        @argumentDefinitions(parent: {type: "String"}) {
            departments(parent: $parent) {
                department {
                    id
                    name
                }
            }
        }
    `
})