import { useRelayEnvironment } from 'react-relay/hooks';
import { Trans } from '@lingui/macro';
import { useAlert } from '@saastack/core';
import { ConfirmContainer } from '@saastack/layouts/containers';
import React from 'react';
import { Variables } from 'relay-runtime';
import DeleteDepartmentMutation from '../mutations/DeleteDepartmentMutation';

interface Props {
    id: string
    variables: Variables
    onClose: () => void
    onSuccess: () => void
}

const DepartmentDelete: React.FC<Props> = ({ variables, id, onClose, onSuccess }) => {
    const showAlert = useAlert()
    const environment = useRelayEnvironment()
    const [loading, setLoading] = React.useState(false)

    const handleDelete = () => {
        setLoading(true)
        DeleteDepartmentMutation.commit(environment, variables, id, {
            onSuccess: handleSuccess,
            onError,
        })
    }
    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }
    const handleSuccess = (id: string) => {
        setLoading(false)
        showAlert(<Trans>Department deleted successfully!</Trans>, {
            variant: 'info',
        })
        onSuccess()
    }

    return (
        <ConfirmContainer
            loading={loading}
            header={<Trans>Delete Designation</Trans>}
            open
            onClose={onClose}
            onAction={(handleDelete)}
        />
    )
}

export default DepartmentDelete;
