import { Trans } from '@lingui/macro'
import { Toggle } from '@saastack/components'
import { Form, Input, Textarea } from '@saastack/forms'
import { FormProps } from '@saastack/forms/types'
import React, { useState } from 'react'

interface Props extends FormProps {
    update?: boolean
}

const DepartmentAddFormComponent: React.FC<Props> = ({ update, ...props }) => {
    const [show, setShow] = useState(false);
    return (
        <Form {...props}>
            <Input large name="name" label={<Trans>Title</Trans>}  grid={{ xs: 12 }}/>
            <Toggle
                label={update ? <Trans>Update Department</Trans> : <Trans>Add Department</Trans>}
                show={show}
                onShow={setShow}
            >
                <Textarea grid={{ xs: 12 }} label={<Trans>Department</Trans>} name="description"/>
            </Toggle>
        </Form>
    );
};

export default DepartmentAddFormComponent;