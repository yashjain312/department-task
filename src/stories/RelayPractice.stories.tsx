import React from 'react';
import DepartmentPage from '../pages/DepartmentPage';
import Wrapper from './Wrapper';

export default {
    title: 'Departments',
    decorators: [(storyFn: () => JSX.Element) =>  <Wrapper>{storyFn()}</Wrapper>], 
}

export const Default = () => <DepartmentPage/>