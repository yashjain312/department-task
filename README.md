### Check List ###

 - [x] UX Match
 - [ ] Roles & Rights
 - [ ] License
 - [x] internationalization
 - [ ] ~~Customer Portal Widget~~
 - [ ] ~~My Space Widget~~
 - [ ] ~~Reports~~
 - [ ] ~~Notifications~~
 - [ ] ~~Notes~~
 - [ ] Policies
 - [x] Responsive
 - [x] Accessibility (list container not accessible)
 - [ ] Activity Log
 - [x] Backward compatibility
 - [x] Check breadcrumb
 - [x] Check app icon in hook register
 - [x] Remove console log
 - [x] Check search bar if used in app (use dounce)
 - [x] Check casing of words
 - [x] Check for validation, internalization in validation messages
 - [x] Default value check in form
 - [x] Active app hooks
 - [x] Default app
 - [x] Check casing in hook
 - [x] Folder structure
 - [x] Remove optimistic updater from delete
 - [x] IconButton wrap in tooltip
 - [x] Refetch on parent change
 - [x] useEffect vs useDidMountEffect 
 - [x] Rename ExpansionPanel to Accordion
 - [x] Font size should be used from typography
 - [x] If you want to remove image, pass src to to image upload component
 - [x] Remove sorting from hooks
 - [x] Add key to item in loop
 - [x] Use useFetchWuery instead of fetchQuery
 - [x] If using fetchQuery, import it from saastack relay

                


### Settings Check List ###

 - [x] UX Match
 - [ ] Roles & Rights
 - [ ] License
 - [x] internationalization
 - [ ] ~~Customer Portal Widget~~
 - [ ] ~~My Space Widget~~
 - [ ] ~~Reports~~
 - [ ] ~~Notifications~~
 - [ ] ~~Notes~~
 - [ ] ~~Policies~~
 - [x] Responsive
 - [x] Accessibility
 - [ ] Activity Log
 - [x] Backward compatibility
 - [x] Check breadcrumb
 - [x] Check app icon in hook register
 - [x] Remove console log
 - [x] Check search bar if used in app (use dounce)
 - [x] Check casing of words
 - [x] Check for validation, internalization in validation messages
 - [x] Default value check in form
 - [x] Active app hooks
 - [ ] ~~Default app~~
 - [x] Check casing in hook
 - [x] Folder structure
 - [x] Remove optimistic updater from delete
 - [x] IconButton wrap in tooltip
 - [x] Refetch on parent change
 - [x] useEffect vs useDidMountEffect 
 - [x] Rename ExpansionPanel to Accordion
 - [x] Font size should be used from typography
 - [x] If you want to remove image, pass src to to image upload component
 - [x] Remove sorting from hooks
 - [x] Add key to item in loop
 - [x] Use useFetchWuery instead of fetchQuery
 - [x] If using fetchQuery, import it from saastack relay


